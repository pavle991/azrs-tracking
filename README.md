# azrs-tracking
Repozitorijum predstavlja prikaz primene alata za razvoj softvera na projektu koji je rađen na kursu "Razvoj softvera".

Projekat na kome su primenjivani alati može se pronaći na sledećem [linku](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno).

# Prikazani alati

1. [Git hooks](https://gitlab.com/pavle991/azrs-tracking/-/issues/1)

2. [GDB](https://gitlab.com/pavle991/azrs-tracking/-/issues/3)

3. [Git](https://gitlab.com/pavle991/azrs-tracking/-/issues/4) (više na [linku](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/12-Uno))

4. [CMake](https://gitlab.com/pavle991/azrs-tracking/-/issues/5)

5. [ClangTidy](https://gitlab.com/pavle991/azrs-tracking/-/issues/7)

6. [ClangFormat](https://gitlab.com/pavle991/azrs-tracking/-/issues/8)

7. [Statička analiza koda - Clazy](https://gitlab.com/pavle991/azrs-tracking/-/issues/9)

8. [Docker](https://gitlab.com/pavle991/azrs-tracking/-/issues/10)

9. [GammaRay](https://gitlab.com/pavle991/azrs-tracking/-/issues/11)

10. [GCov](https://gitlab.com/pavle991/azrs-tracking/-/issues/12)

# Sistem za versionisanje softvera

Na projektu iz "Razvoja softvera" koristili smo Git (GitFlow)
